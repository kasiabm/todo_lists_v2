User.destroy_all
Profile.destroy_all
TodoList.destroy_all
TodoItem.destroy_all

user1 = User.create!(username: "Fiorina", password_digest: "aniroiF4591")
user2 = User.create!(username: "Trump", password_digest: "pmurt6491")
user3 = User.create!(username: "Carson", password_digest: "nosrac1591")
user4 = User.create!(username: "Clinton", password_digest: "notnilc7491")

user1.create_profile!(gender: "female", birth_year: 1954, first_name: "Carly", last_name: "Fiorina")
user2.create_profile!(gender: "male", birth_year: 1946, first_name: "Donald", last_name: "Trump")
user3.create_profile!(gender: "male", birth_year: 1951, first_name: "Ben", last_name: "Carson")
user4.create_profile!(gender: "female", birth_year: 1947, first_name: "Hillary", last_name: "Clinton")

user1.todo_lists.create!(list_name: "sell ponies", list_due_date: Date.today + 1.year)
user2.todo_lists.create!(list_name: "finish high school", list_due_date: Date.today + 1.year)
user3.todo_lists.create!(list_name: "dig out family bones", list_due_date: Date.today + 1.year)
user4.todo_lists.create!(list_name: "get plastic ops", list_due_date: Date.today + 1.year)

user1.todo_lists.first.todo_items.create!(due_date: Date.today + 1.year, title: "george", description: "sell George")
user1.todo_lists.first.todo_items.create!(due_date: Date.today + 1.year, title: "stan", description: "sell Stan")
user1.todo_lists.first.todo_items.create!(due_date: Date.today + 1.year, title: "amanda", description: "sell Amanda")
user1.todo_lists.first.todo_items.create!(due_date: Date.today + 1.year, title: "darby", description: "sell Darby")
user1.todo_lists.first.todo_items.create!(due_date: Date.today + 1.year, title: "osborne", description: "sell Osborne")
user2.todo_lists.first.todo_items.create!(due_date: Date.today + 1.year, title: "maths", description: "do maths exam")
user2.todo_lists.first.todo_items.create!(due_date: Date.today + 1.year, title: "english", description: "do english exam")
user2.todo_lists.first.todo_items.create!(due_date: Date.today + 1.year, title: "pe", description: "do pe exam")
user2.todo_lists.first.todo_items.create!(due_date: Date.today + 1.year, title: "arts", description: "do arts exam")
user2.todo_lists.first.todo_items.create!(due_date: Date.today + 1.year, title: "dutch", description:  "do dutch exam")
user3.todo_lists.first.todo_items.create!(due_date: Date.today + 1.year, title: "grandma", description: "dig out grandma")
user3.todo_lists.first.todo_items.create!(due_date: Date.today + 1.year, title: "great grandma", description: "dig out great grandma")
user3.todo_lists.first.todo_items.create!(due_date: Date.today + 1.year, title: "great great grandma", description: "dig out great great grandma")
user3.todo_lists.first.todo_items.create!(due_date: Date.today + 1.year, title: "uncle", description: "dig out uncle")
user3.todo_lists.first.todo_items.create!(due_date: Date.today + 1.year, title: "ex", description: "dig out ex-wife")
user4.todo_lists.first.todo_items.create!(due_date: Date.today + 1.year, title: "boobs", description: "get boobs done")
user4.todo_lists.first.todo_items.create!(due_date: Date.today + 1.year, title: "arse", description: "get arse done")
user4.todo_lists.first.todo_items.create!(due_date: Date.today + 1.year, title: "chin", description: "get chin done")
user4.todo_lists.first.todo_items.create!(due_date: Date.today + 1.year, title: "nose", description: "get nose done")
user4.todo_lists.first.todo_items.create!(due_date: Date.today + 1.year, title: "lipo", description: "get lipo done")